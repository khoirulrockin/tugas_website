import React, { Component } from "react";
import './BlogPost.css';
import Mahasiswa from "../../../../modul4_tugas/src/component/Mahasiswa";

class Forms extends Component {
    state = {
        listMahasiswa: [],
        insertMahasiswa: {
            id: 1,
            nim: null,
            nama: "",
            alamat: "",
            hp: "",
            angkatan: null,
            status: ""
        }
    }

    ambilDataDariServerAPI = () => {
        fetch('http://localhost:3001/mahasiswa?_sort=nim&_order=desc')
            .then(response => response.json())
            .then(jsonHasilAmbilDariAPI => {
                this.setState({
                    listMahasiswa: jsonHasilAmbilDariAPI
                })
            })
    }

    componentDidMount() {
        this.ambilDataDariServerAPI()
    }

    handleHapusMahasiswa = (data) => {
        fetch(`http://localhost:3001/mahasiswa/${data}`, { method: 'DELETE' })
            .then(res => {
                this.ambilDataDariServerAPI()
            })
    }

    handleTambahMahasiswa = (event) => {
        let formInsertMahasiswa = { ...this.state.insertMahasiswa };
        let timestamp = new Date().getTime();
        formInsertMahasiswa['id'] = timestamp;
        formInsertMahasiswa[event.target.name] = event.target.value;
        this.setState({
            insertMahasiswa: formInsertMahasiswa
        });
    }

    handleTombolSimpan = () => {
        fetch('http://localhost:3001/mahasiswa/', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state.insertMahasiswa)
        })
            .then((Response) => {
                this.ambilDataDariServerAPI();
            })
    }

    render() {
        return (
            <div className="post-mahasiswa">
                <div className="form pb-2 border-bottom">
                    <div class="row">
                        <div class="col-lg-6 mb-3">
                            <label for="inputNama" class="form-label">NIM</label>
                            <input type="text" class="form-control form-control-user2" id="inputNama" name="nim"
                                placeholder="Ex: 41200320" required data-parsley-required-message="Data harus di isi berupa angka !!!"
                                onChange={this.handleTambahMahasiswa} />
                        </div>
                        <div class="col-lg-6 mb-3">
                            <label for="inputNama" class="form-label">Alamat</label>
                            <input type="text" class="form-control form-control-user2" id="inputNama" name="alamat"
                                placeholder="Ex: Jl. Dharmawangsa No.10, Patrang, Jember" required data-parsley-required-message="Data harus di isi berupa angka !!!"
                                onChange={this.handleTambahMahasiswa} />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 mb-3">
                            <label for="inputNama" class="form-label">Nama</label>
                            <input type="text" class="form-control form-control-user2" id="inputNama" name="nama"
                                placeholder="Ex: Budi Santoso" required data-parsley-required-message="Data harus di isi !!!"
                                onChange={this.handleTambahMahasiswa} />
                        </div>
                        <div class="col-lg-6 mb-3">
                            <label for="inputNama" class="form-label">Angkatan</label>
                            <input type="text" class="form-control form-control-user2" id="inputNama" name="angkatan"
                                placeholder="Ex: 2020" required data-parsley-required-message="Data harus di isi berupa angka !!!"
                                onChange={this.handleTambahMahasiswa} />
                        </div>                        
                    </div>
                    <div class="row">
                        <div class="col-lg-6 mb-3">
                            <label for="inputNama" class="form-label">Nomor Handphone</label>
                            <input type="text" class="form-control form-control-user2" id="inputNama" name="hp"
                                placeholder="Ex: 085808241204" required data-parsley-required-message="Data harus di isi !!!"
                                onChange={this.handleTambahMahasiswa} />
                        </div>
                        <div class="col-lg-6 mb-3">
                            <label for="inputNama" class="form-label">Status</label>
                            <input type="text" class="form-control form-control-user2" id="inputNama" name="status"
                                placeholder="Ex: Aktif" required data-parsley-required-message="Data harus di isi !!!"
                                onChange={this.handleTambahMahasiswa} />
                        </div>
                    </div>
                    
                    <botton type="submit" className="btn btn-primary" onClick={this.handleTombolSimpan}>Simpan</botton>
                    
                </div>
                <h2>Daftar Mahasiswa</h2>
                {
                    this.state.listMahasiswa.map(mahasiswa => {
                        return <Mahasiswa id={mahasiswa.id} nim={mahasiswa.nim} nama={mahasiswa.nama} alamat={mahasiswa.alamat} hp={mahasiswa.hp} angkatan={mahasiswa.angkatan} status={mahasiswa.status} hapusMahasiswa={this.handleHapusMahasiswa} />
                    })}
            </div>
        )
    }
}

export default Forms;