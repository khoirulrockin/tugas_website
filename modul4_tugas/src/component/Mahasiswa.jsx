import React from "react";

const Mahasiswa = (props) => {
    return (
        <div className="card">
            {/* <div className="images">
                <img src="http://placeimg.com/80/80/tech" alt="Gambar Tumbnail Artikel" />
            </div> */}
            <div className="card-body">
                <div className="card-title">Nama : {props.nama}</div>
                <p className="card-text">NIM : {props.nim}</p>
                <p className="card-text">Alamat : {props.alamat}</p>
                <p className="card-text">Nomor Handphone : {props.hp}</p>
                <p className="card-text">Angkatan : {props.angkatan}</p>
                <p className="card-text">Status : {props.status}</p>
                <button className="btn btn-sm btn-warning" onClick={() => props.hapusMahasiswa(props.id)}>Hapus</button>
            </div>
        </div>
    )
}

export default Mahasiswa;