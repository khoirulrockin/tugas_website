import React, { Component } from "react";
import Colpost from "./Colpost";

class Coller extends React.Component{
    state = {
        notes: [],
        insertnotes: {
        "id_note": 0,
        "uid": 0,
        "judul_note": "",
        "tgl_note": "",
        "isi_note": ""
    }}


    // get notes
    fetchNote = () => {
        fetch('http://localhost:3001/notes')
            .then(response => response.json())
            .then(json => {
                this.setState({
                    notes: json
                })
            })
    }

    // delete note by id
    deleteNote = (id_note) => {
        fetch('http://localhost:3001/notes/' + id_note, {
            method: 'DELETE'
        })
            .then(json => this.fetchNote())
    }
    

    // handle value of field
    handleChangeInsert = (event) => {
        let formInsertNotes = { ...this.state.insertnotes };
        formInsertNotes['id_note'] = new Date().getTime();
        formInsertNotes[event.target.name] = event.target.value;
        this.setState({
            insertnotes: formInsertNotes
        });
    }

    // insert to API
    insertnote = () => {
        fetch('http://localhost:3001/notes', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state.insertnotes)
        })
            .then(response => response.json())
            .then(json => this.fetchNote())
    }

    componentDidMount() {
        this.fetchNote();
    }

    render() {
        return (
            <div className="container-fluid p-4">
                <div className="row">
                    <div className="col-md-4 border p-4 me-4">
                        <h3>Tambah Note</h3>

                        <form>
                            {/* title */}
                            <div className="mb-3">
                                <label htmlFor="judul_note" className="form-label">Judul Note</label>
                                <input type="text" className="form-control" id="judul_note" name="judul_note" onChange={this.handleChangeInsert} />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="tgl_note" className="form-label">Tanggal</label>
                                <input type="text" className="form-control" id="tgl_note" name="tgl_note" onChange={this.handleChangeInsert} />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="isi_note" className="form-label">Isi</label>
                                <input type="text" className="form-control" id="isi_note" name="isi_note" onChange={this.handleChangeInsert} />
                            </div>
                            <div className="d-grid">
                                <button className="btn btn-secondary" onClick={this.insertnote}>Tambah Note</button>
                            </div>
                        </form>

                    </div>
                    <div className="col-md border p-4">
                        <h3> Daftar Note</h3>
                        <div className="row">
                            {
                                this.state.notes.map(note => {
                                    return <Colpost key={note.id_note} uid={note.uid} title={note.judul_note} tanggal={note.tgl_note} isi={note.isi_note} id={note.id_note} delete={this.deleteNote} />
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Coller;
