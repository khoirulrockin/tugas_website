import React from "react";

function Colpost(props) {
    return (
        <div className="card col-md-4 mb-3 me-3">
            {/* <img src={props.image} className="card-img-top" width={100} height={150} alt="Gambarnya"></img> */}
            <div className="card-body">
                <h5 className="card-title">{props.title}</h5>
                <h6 className="card-subtitle mb-2 text-muted">Tanggal: {props.tanggal}</h6>
                <p className="card-text">Isi {props.isi}</p>
                <button className="btn btn-danger" onClick={
                    () => {
                        props.delete(props.id_notes)
                    }
                }>Hapus</button>
            </div>
        </div>
    );
}

export default Colpost;