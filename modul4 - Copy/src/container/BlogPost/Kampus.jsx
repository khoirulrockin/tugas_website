import React, { Component } from "react";
import KamPost from "./KamPost";

class Kampus extends Component {
    state = {               //komponen state dari react untuk statefull component
        dataMahasiswa: [],
        insertdata: {
            "NIM": 0,
            "nama": "",
            "alamat": "",
            "hp": "",
            "angkatan": "",
            "status": ""
        }
    }

    //get data from api
    getDataAPI = () => {
        fetch('http://localhost:3001/mahasiswa')
            .then(response => response.json())
            .then(json => {
                this.setState({
                    dataMahasiswa: json
                })
            })
    }

    componentDidMount() {
        this.getDataAPI()
    }

    //handle btn hapus
    btnHapus = (NIM) => {
        fetch('http://localhost:3001/mahasiswa/' + NIM,
            { method: 'DELETE' })
            .then(json => {
                this.getDataAPI()
            })
    }


    render() {
        return (
            <div className="container-fluid ">
                <div className="row">
                    <div className="col-md-4 border p-4 me-4">
                        <h3>Tambah Data Mahasiswa</h3>

                        <form>
                            {/* title */}
                            <div className="mb-3 ">
                                <label htmlFor="judul_note" className="form-label">NIM</label>
                                <input type="text" className="form-control" id="NIM" name="NIM" onChange={this.handleChangeInsert} />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="nama" className="form-label">Nama</label>
                                <input type="text" className="form-control" id="nama" name="nama" onChange={this.handleChangeInsert} />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="alamat" className="form-label">Alamat</label>
                                <input type="text" className="form-control" id="alamat" name="alamat" onChange={this.handleChangeInsert} />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="hp" className="form-label">No HP</label>
                                <input type="text" className="form-control" id="hp" name="hp" onChange={this.handleChangeInsert} />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="angkatan" className="form-label">No HP</label>
                                <input type="text" className="form-control" id="angkatan" name="angkatan" onChange={this.handleChangeInsert} />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="status" className="form-label">No HP</label>
                                <input type="text" className="form-control" id="status" name="status" onChange={this.handleChangeInsert} />
                            </div>
                            <div className="d-grid">
                                <button className="btn btn-secondary" onClick={this.insertnote}>Tambah Data</button>
                            </div>
                        </form>
                           

                        </div>
                        <div className="col-md col-mr-4 border p-4">
                            <h3 className="text-center mb-4"> Daftar Mahasiswa</h3>
                            <div className="row  ">
                                {
                                    this.state.dataMahasiswa.map(datamhs => {
                                        return <KamPost key={datamhs.NIM} NIM={datamhs.NIM} nama={datamhs.nama} alamat={datamhs.alamat} hp={datamhs.hp} angkatan={datamhs.angkatan} status={datamhs.status} hapusData={this.btnHapus} />
                                    })
                                }
                            
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Kampus;