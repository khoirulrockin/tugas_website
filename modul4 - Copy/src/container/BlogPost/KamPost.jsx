import React from "react";

const KamPost = (props) => {
    return (
        <div className="card col-md-4 mb-4 ml-4 me-4">
            <div className="card-body">
                <h5 className="card-title">NIM : {props.NIM}</h5>
                <h6 className="card-subtitle mb-2 text-muted">Nama : {props.nama}</h6>
                <p className="card-text">Alamat : {props.alamat}</p>
                <p className="card-text">No hp : {props.hp}</p>
                <p className="card-text">Angkatan : {props.angkatan}</p>
                <p className="card-text">Status : {props.status}</p>
                 <div className="d-grid">
                 <button className="btn btn-warning" onClick={() => props.hapusData(props.NIM)}>Hapus</button>
                </div> 
            </div>
        </div>
    )
}
export default KamPost;