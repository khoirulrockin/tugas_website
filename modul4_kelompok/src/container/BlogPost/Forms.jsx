import React, { Component } from "react";
import './BlogPost.css';
import StudioFoto from "../../component/StudioFoto";

class Forms extends Component {
    state = {
        bs_access: [],
        insertStudio: {
            id: 1,
            studio : "",
            alamat: "",
            deskripsi: "",
            hp: "",
            harga: 1
        }
    }

    ambilDataDariServerAPI = () => {
        fetch('http://localhost:3001/studio?_sort=id&_order=desc')
            .then(response => response.json())
            .then(jsonHasilAmbilDariAPI => {
                this.setState({
                    bs_access: jsonHasilAmbilDariAPI
                })
            })
    }

    componentDidMount() {
        this.ambilDataDariServerAPI()
    }

    delete_studio = (data) => {
        fetch(`http://localhost:3001/studio/${data}`, { method: 'DELETE' })
            .then(res => {
                this.ambilDataDariServerAPI()
            })
    }

    insertStudio = (event) => {
        let formInsertStudio = { ...this.state.insertStudio };
        let timestamp = new Date().getTime();
        formInsertStudio['id'] = timestamp;
        formInsertStudio[event.target.name] = event.target.value;
        this.setState({
            insertStudio: formInsertStudio
        });
    }

    button_save = () => {
        fetch('http://localhost:3001/studio/', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state.insertStudio)
        })
            .then((Response) => {
                this.ambilDataDariServerAPI();
            })
    }

    render() {
        return (
            <div className="post-studio">
                <div className="form pb-2 border-bottom">
                    <div class="row">
                        <div class="col-lg-3 mb-3">
                            <label for="inputNama" class="form-label">Nama Studio</label>
                            <input type="text" class="form-control form-control-user2" id="inputNama" name="studio"
                                placeholder="Ex: King Foto" required data-parsley-required-message="Data harus di isi berupa angka !!!"
                                onChange={this.insertStudio} />
                        </div>
                        <div class="col-lg-6 mb-3">
                            <label for="inputNama" class="form-label">Alamat</label>
                            <input type="text" class="form-control form-control-user2" id="inputNama" name="alamat"
                                placeholder="Ex: Jl. Dharmawangsa No.10, Patrang, Jember" required data-parsley-required-message="Data harus di isi berupa angka !!!"
                                onChange={this.insertStudio} />
                        </div>
                    </div>
                    <div class="row">                        
                        <div class="col-lg-3 mb-3">
                            <label for="inputNama" class="form-label">Handphone</label>
                            <input type="text" class="form-control form-control-user2" id="inputNama" name="hp"
                                placeholder="Ex: 085808241204" required data-parsley-required-message="Data harus di isi berupa angka !!!"
                                onChange={this.insertStudio} />
                        </div>
                        <div class="col-lg-6 mb-3">
                            <label for="inputNama" class="form-label">Deskripsi</label>
                            <input type="text" class="form-control form-control-user2" id="inputNama" name="deskripsi"
                                placeholder="Ex: Deskripsi dari studio" required data-parsley-required-message="Data harus di isi !!!"
                                onChange={this.insertStudio} />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 mb-3">
                            <label for="inputNama" class="form-label">Harga</label>
                            <input type="text" class="form-control form-control-user2" id="inputNama" name="harga"
                                placeholder="Ex: 100000" required data-parsley-required-message="Data harus di isi !!!"
                                onChange={this.insertStudio} />
                        </div>
                    </div>
                    
                    <botton type="submit" className="btn btn-primary" onClick={this.button_save}>Simpan</botton>
                    
                </div>
                <h2>Daftar Studio</h2>
                {
                    this.state.bs_access.map(studio => {
                        return <StudioFoto id={studio.id} studio={studio.studio} alamat={studio.alamat} deskripsi={studio.deskripsi} hp={studio.hp} harga={studio.harga} delete_studio={this.delete_studio} />
                    })}
            </div>
        )
    }
}

export default Forms;