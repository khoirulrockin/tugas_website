import React from "react";

const StudioFoto = (props) => {
    return (
        <div className="card">
            {/* <div className="images">
                <img src="http://placeimg.com/80/80/tech" alt="Gambar Tumbnail Artikel" />
            </div> */}
            <div className="card-body">
                <h4 className="card-title">{props.studio}</h4>
                <p className="card-text">{props.deskripsi}</p>
                <p className="card-text">{props.alamat}</p>
                <p className="card-text">Contact : {props.hp} | Rp. {props.harga} /jam</p>
                <button className="btn btn-sm btn-warning" onClick={() => props.delete_studio(props.id)}>Hapus</button>
            </div>
        </div>
    )
}

export default StudioFoto;