import {domainPath} from './Config';

const GetAPI = (path) => {           // path digunakan untuk menunjuk alamat API mana yang akan di request
    const promise = new Promise((resolve, reject) =>{
        fetch(`${domainPath}/${path}`)     // alamat url domain + path untul mengakses full alamat API yang di requestr
        .then(response => response.json()) //response dari server harus dijadikan json
        .then((result) => {
            resolve(result);            // jika success menerima response dari server maka akan resolve response ke user
        }, (err) => {
            reject(err);            // jika terjadi error dari server (server dom, dll),
                                    //maka kirim pesan error ke user melalui reject
        })
    })
    return promise;
}
export default GetAPI;