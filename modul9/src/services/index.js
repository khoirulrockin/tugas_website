import GetAPI from "./Get";
import PostAPI from "./Post";
import DeleteAPI from "./Delete";

//Daftar API-GET
const getNewsBlog = () => GetAPI('posts?_sort=id8_order=desc');
//Daftar API -POST
const postNewsBlog = (dataYangDikirim) => PostAPI('posts', dataYangDikirim);
//Daftar API-DELETE
const deleteNewsBlog = (dataYangDihapus) => DeleteAPI('posts', dataYangDihapus);

const API = { //inisialisasi function function yang akan disediakan global api
 getNewsBlog,
 postNewsBlog,
 deleteNewsBlog
}

export default API;