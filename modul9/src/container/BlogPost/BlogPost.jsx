import React, { Component } from 'react';
import './BlogPost.css';
import Post from './Post';
import API from '../../services';


class BlogPost extends Component {
    state = {               //komponen state dari react untuk statefull component
        listArtikel: [],     //variable array yang digunakan untuk menyimpan data API
        inserArtikel: {
            userId: 1,
            id: 1,
            title: "",
            body: ""
        }
    }

    // menampilkan data 
    ambilDataDariServerAPI = () => {    //komponen untuk mengecek ketika component telah di mount-ing, maka panggil API
        API.getNewsBlog().then(result => {
            this.setState({
                listArtikel: result
            })
        }) //alamat URL API yang ingin kita ambil datanya

    }

    componentDidMount() {
        this.ambilDataDariServerAPI()
    }

    //menghapus data 
    handleHapusArtikel = (data) => {
        API.deleteNewsBlog(data).then((response) => {
            this.ambilDataDariServerAPI();
        });
    }

    handleTambahArtikel = (event) => {
        let formInsertArtikel = { ...this.state.inserArtikel };
        let timestamp = new Date().getTime();
        formInsertArtikel['id'] = timestamp;
        formInsertArtikel[event.target.name] = event.target.value;
        this.setState({
            inserArtikel: formInsertArtikel
        });
    }

    //insert to API
    handleTombolSimpan = () => {
        API.postNewsBlog(this.state.inserArtikel)
            .then((response) => {
                this.ambilDataDariServerAPI();
            });

    }

    render() {
        return (
            <div className="post-artikel">
                <div className='form pb-2 border-bottom'>
                    <div className='form-group row'>
                        <label htmlFor='title' className='col-sm-2 col-form-label'>Judul</label>
                        <div className='col-sm-10'>
                            <input type='text' className='form-control' id='title' name='title' onChange={this.handleTambahArtikel} />
                        </div>
                    </div>
                    <div className='form-group-row'>
                        <label htmlFor='body' className='col-sm-2 col-form-label'>Isi</label>
                        <div className='col-sm-10'>
                            <textarea className='form-control' id='body' name='body' rows='3' onChange={this.handleTambahArtikel}></textarea>
                        </div>
                    </div>
                    <div>
                        <button type='submit' className='btn btn-primary' onClick={this.handleTombolSimpan}>Simpan</button>
                    </div>
                </div>
                <h2>Daftar Artikel</h2>
                {
                    this.state.listArtikel.map(artikel => { //looping dan masukkan untuk setiap data yang ada di listArtikel ke var artikel
                        return <Post key={artikel.id} judul={artikel.title} isi={artikel.body} idArtikel={artikel.id} hapusArtikel={this.handleHapusArtikel} /> //mapping data json dari APi sesuaai dengan kategorinya
                    })
                }

            </div>

        )
    }
}

export default BlogPost;