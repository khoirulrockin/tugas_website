import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import BlogPost from './container/BlogPost/BlogPost';
import Coller from './container/BlogPost/Coller';
import Kampus from './container/BlogPost/Kampus';

ReactDOM.render(
    <BlogPost />,
  document.getElementById('content')
);

// If you want to start measuring performance ffin your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
